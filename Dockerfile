FROM debian:jessie

RUN awk '$1 ~ "^deb" { $3 = $3 "-backports"; print; exit }' /etc/apt/sources.list > /etc/apt/sources.list.d/backports.list
RUN awk '$1 ~ "^deb" { $3 = "sid"; print; exit }' /etc/apt/sources.list > /etc/apt/sources.list.d/sid.list

ENV DEBIAN_FRONTEND=noninteractive

RUN mkdir -p /opt/py-webserver/upload && ln -s /opt/py-webserver/upload/ /opt/py-webserver/delete

	
#copy tools
COPY webserver.py /opt/py-webserver/
COPY default.txt /opt/py-webserver/upload

USER root
RUN	apt-get update && apt-get install -y --no-install-recommends python3 && \
	rm -rf /var/lib/apt/lists/*  && \
	apt-get autoremove -y && apt-get clean && \ 
	chmod a+x -R /opt/py-webserver/

EXPOSE 8080

CMD cd /opt/py-webserver && python3 webserver.py $PORT_NUMBER