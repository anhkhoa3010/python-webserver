-<b>How to build webserver docker</b>
  - Checkout source code 
  - You can use two options to build/start docker file
    - Grant execute permission for run.sh and then execute below command:
      - run.sh build: to build docker image
      - run.sh run PORT_NUMBER: to run image as a container with port number parameter
      - run.sh deploy: to push image to private registry. Please correct private server first
      - run.sh clean: to clean the container
      - run.sh start: to start an exist webserver container
      - run.sh stop: to stop the container
      - run.sh delete: to delete the images
    - Use docker-compose.yml 

-<b>How to upload file</b>
  - Use curl command with -F option to upload file: 
    - Exmaple: <i>curl -XPOST http://<server_name>:<port_number>/upload -H 'Content-Type: text/plain' -F 'file=@D:\path_filename1' -F 'file=@path_filename2' </i>
  - Retrieve data: you can use browser to view/download the uploaded file or use curl command
    - Example: <i>curl -XGET http://<server_name>:<port_number>/filename</i>
	

-<b>How to delete file</b>
  - Use curl command with POST method: 
    - Exmaple: <i>curl -XPOST http://<server_name>:<port_number>/delete/filename </i>
    - Exmaple: <i>curl -XPOST http://<server_name>:<port_number>/delete?filename=filename1?filename=filename2 </i>
  - Use browser to deleted the uploaded file or use curl command with GET method
    - Example: <i>curl -XGET http://<server_name>:<port_number>/delete/filename </i>
    - Example: <i>curl -XGET http://<server_name>:<port_number>/delete?filename=filename1?filename=filename2 </i>