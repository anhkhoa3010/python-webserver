#!/bin/bash
scenario=$1


CONTAINER_NAME="py-webserver"
IMAGE_NAME="webserver"
IMAGE_VERSION="1.0"
PRIVATE_DOCKER="anhkhoa-lap:18442"



if [ $# -gt 2 ]; then
    echo $0: usage: Enter Scenario to run
    exit 1
fi

echo $scenario

if [ "$scenario" == "build" ]; then #This scenario is used to build Image from dockerfile

	IMAGE_ID=$(docker build --force-rm=true --no-cache=true -t $IMAGE_NAME:$IMAGE_VERSION .)
	if ["$IMAGE_ID" == ""]; then
		echo "Failed! Please fix it & build again"
		docker rmi $(docker images -f "dangling=true" -q)
		exit 1
	else
		echo "Build successfully"
	fi
elif [ "$scenario" == "run" ]; then #This scenario is used to run Image as container
	if [ "$2" == "" ]; then
		PORT_NUMBER=8080
	else
		PORT_NUMBER=$2
	fi
	docker run -d -p $PORT_NUMBER:$PORT_NUMBER -e PORT_NUMBER=$PORT_NUMBER --name $CONTAINER_NAME $IMAGE_NAME:$IMAGE_VERSION
elif [ "$scenario" == "deploy" ]; then #This scenario is used to deploy the Image to private docker server
	TIME_TAG=$(date +%s)
	docker tag $IMAGE_NAME:$IMAGE_VERSION $PRIVATE_DOCKER/$IMAGE_NAME:$IMAGE_VERSION-$TIME_TAG
	docker push $PRIVATE_DOCKER/$IMAGE_NAME:$IMAGE_VERSION-$TIME_TAG
	docker rmi $PRIVATE_DOCKER/$IMAGE_NAME:$IMAGE_VERSION-$TIME_TAG
elif [ "$scenario" == "clean" ]; then #This scenario delete the container
	CONTAINER_ID=$(docker ps -q -f name=$CONTAINER_NAME -f status=exited)
	if [ -n "$CONTAINER_ID" ]; then
      docker rm -f $CONTAINER_ID
    else
      echo "Container didn't exist. Cannot clean"
    fi
	
elif [ "$scenario" == "stop" ]; then #This scenario stop the container
	CONTAINER_ID=$(docker ps -q -f name=$CONTAINER_NAME -f status=running)
	if [ -n "$CONTAINER_ID" ]; then
      docker stop $CONTAINER_ID
    else
      echo "Container didn't run. Cannot stop"
    fi
elif [ "$scenario" == "start" ]; then #This scenario is used to start the exist container
	CONTAINER_ID=$(docker ps -q -f name=$CONTAINER_NAME -f status=exited)
	if [ -n "$CONTAINER_ID" ]; then
      docker start $CONTAINER_ID
    else
      echo "Container is running or not exist. Cannot start"
    fi
elif [ "$scenario" == "delete" ]; then #This scenario delete the  Image
	IMAGE_ID=$(docker images $IMAGE_NAME:$IMAGE_VERSION)
	if [ -n "$IMAGE_ID" ]; then
      docker rmi $IMAGE_NAME:$IMAGE_VERSION
    else
      echo "Image didn't exist. Cannot delete"
    fi
else
    echo "Please enter the correct scenario"
	echo "    - build: to build docker image"
	echo "    - run PORT_NUMBER: to run image as a container with port_number"
	echo "    - deploy: to push image to private registry. Please correct private server first"
	echo "    - clean: to clean the container"
	echo "    - stop: to stop the container"
	echo "	  - delete: to delete the image"
fi
