#!/usr/bin/python
import http.server
from os import curdir
import os
import re
from sys import argv
import os.path
import cgi
import re

try:
        from StringIO import StringIO
except ImportError:
        from io import StringIO

class myHandler(http.server.BaseHTTPRequestHandler):

        def delete_File(self):
                filepath = os.getcwd() + self.path
                filename = os.path.basename(self.path)
                if os.path.exists(filepath):
                        os.remove(filepath)
                        self.send_response(200)
                        self.end_headers()
                        msg = filename + " already deleted"
                        self.wfile.write("{}".format(msg).encode('utf-8'))
                        
                else:
                        self.send_error(404,'File Not Found: %s' % filename)
        
        def delete_File_Parameter(self):
                print ("Delete file as parameter")
                notExistedFile = ""
                files = []
                filesExist = True
                for parameter in self.path.split("?"):
                        if not parameter.startswith("/delete"):
                                fn = parameter.split("=")[1]
                                filepath = os.getcwd() + "/delete/" + fn
                                files.append(fn)
                                if not os.path.exists(filepath):
                                        filesExist = False
                                        notExistedFile = notExistedFile+" " + fn

                if filesExist == True:
                        for fn in files:
                                print ("File name is %s" % fn)
                                filepath = os.getcwd() + "/delete/" + fn
                                os.remove(filepath)
                        msg= "File(s) already deleted: " + " ".join(files)
                        self.send_response(200)
                        self.end_headers()
                        self.wfile.write("{}".format(msg).encode('utf-8'))
                else:
                        self.send_error(404,'File Not Found: %s' % notExistedFile)

                

        #Handler for the GET requests
        def do_GET(self):
                regDelete = re.compile("^/delete/[\w,\s-]+\.[A-Za-z]{3}$")
                regDeleteParamter = re.compile("^/delete(\?filename=[\w,\s-]+\.[A-Za-z]{3})+$")

                if regDelete.match(self.path):
                        self.delete_File()
                        return
                if regDeleteParamter.match(self.path):
                        self.delete_File_Parameter()
                        return
                if self.path=="/":
                        self.path="/default.txt"

                try:
                        #set the right mime type
                        mimetype = 'application/octet-stream'
                        if self.path.endswith(".html"):
                                mimetype='text/html'

                        if self.path.endswith(".jpg"):
                                mimetype='image/jpg'

                        if self.path.endswith(".gif"):
                                mimetype='image/gif'

                        if self.path.endswith(".js"):
                                mimetype='application/javascript'

                        if self.path.endswith(".css"):
                                mimetype='text/css'

                        if self.path.endswith(".txt"):
                                mimetype='text/plain'

                        #Open the static file requested and send it
                        fn = curdir + "/upload" + self.path
                        print ("Retrieve file %s" % fn)
                        f = open(fn, 'rb')
                        self.send_response(200)
                        self.send_header('Content-type',mimetype)
                        self.end_headers()
                        self.wfile.write(f.read())
                        f.close()
                        return

                except IOError:
                        self.send_error(404,'File Not Found: %s' % self.path)

        def read_File(self):
                content_type = self.headers['content-type']
                files = ""
                if not content_type:
                        return (False, "Content-Type header doesn't contain boundary")
                boundary = content_type.split("=")[1].encode()
                remainbytes = int(self.headers['content-length'])
                line = self.rfile.readline()
                remainbytes -= len(line)
                if remainbytes == 0:
                        return (False, "Don't have file uploaded")
                if not boundary in line:
                        return (False, "Content NOT begin with boundary")
                fn = os.path.join(curdir, "tmp.txt")
                try:
                        out = open(fn, 'wb')
                except IOError:
                        return (False, "Can't opem file to write")
                        
                while remainbytes> 0:
                        if boundary in line:
                                out.close()
                                print ("End save %s file" % fn)
                                line = self.rfile.readline()
                                remainbytes -= len(line)
                                fn = re.findall(r'Content-Disposition.*name="file"; filename="(.*)"', line.decode())
                                files += " " + fn[0]
                                fn = os.path.join(curdir, "upload", fn[0])
                                try:
                                        out = open(fn, 'wb')
                                        print ("Begin save %s file" % fn)
                                except IOError:
                                        return (False, "Can't open file to write")
                                line = self.rfile.readline()
                                remainbytes -= len(line)
                                line = self.rfile.readline()
                                remainbytes -= len(line)
                        else:
                                out.write(line)

                        line = self.rfile.readline()
                        remainbytes -= len(line)
                out.close()
                print ("End save %s file" % fn)
                return (False, "File(s) upload successful: %s" % files)


        #Handler for the POST requests
        def do_POST(self):
                regDelete = re.compile("^/delete/[\w,\s-]+\.[A-Za-z]{3}$")
                regDeleteParamter = re.compile("^/delete(\?filename=[\w,\s-]+\.[A-Za-z]{3})+$")

                if self.path.startswith("/upload"):
                        r, info = self.read_File()
                        self.send_response(200)
                        self.end_headers()
                        self.wfile.write("{}".format(info).encode('utf-8'))
                if regDelete.match(self.path):
                        self.delete_File()
                        return
                if regDeleteParamter.match(self.path):
                        self.delete_File_Parameter()
                        return
                        
                        
try:
        ###Create webserver
        if len(argv) == 2:
                PORT_NUMBER=int(argv[1])
        else:
                PORT_NUMBER=8080
        server = http.server.HTTPServer(('', PORT_NUMBER), myHandler)
        print ('Started httpserver on port %s ' % PORT_NUMBER)
        
        ###Wait forever
        server.serve_forever()

except KeyboardInterrupt:
        print ('Ctrl+C to shutdown the webserver')
        server.socket.close()
